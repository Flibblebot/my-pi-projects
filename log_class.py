#!/usr/bin/python
# --------------------------------------
#
#  log_class.py
#  Class to setup and manage log files and standard formatting from one place.
#
# TO DO:
#
#  Author    : Andy Fair
#  Updated   : 16/06/2016
# --------------------------------------
# imports
useConfigParser = False
try:
    import configparser
except ImportError:
    import ConfigParser
    useConfigParser = True
import logging
import logging.handlers

class Log:
    CRITICAL = logging.CRITICAL
    ERROR = logging.ERROR
    WARNING = logging.WARNING
    INFO = logging.INFO
    DEBUG = logging.DEBUG

    log_level = None

    def __init__(self):
        if (useConfigParser == True):
            cp = ConfigParser.ConfigParser()
        else:
            cp = configparser.ConfigParser()

        cp.read("/home/pi/heating/heating.cfg")
        self.log_level = getattr(logging, cp.get('general', 'LOG_LEVEL').upper(), None)

    def log_message(self, message, level):
        logger = logging.getLogger('htg_log')
        logger.setLevel(self.log_level)

        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s', '%H:%M:%S')

        handler = logging.handlers.RotatingFileHandler('/home/pi/heating/logs/heating.log', maxBytes=1048576, backupCount=5)
        handler.setLevel(self.log_level)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        #fh = logging.FileHandler('/home/pi/heating/heating.log')
        #fh.setLevel(logging.DEBUG)
        #fh.setFormatter(formatter)
        #logger.addHandler(fh)

        if (level == self.CRITICAL):
            logger.critical(message)
        elif (level == self.ERROR):
            logger.error(message)
        elif (level == self.WARNING):
            logger.warning(message)
        elif (level == self.INFO):
            logger.info(message)
        elif (level == self.DEBUG):
            logger.debug(message)

        logger.removeHandler(handler)
        handler.close()
