#!/usr/bin/python
# --------------------------------------
#
#  lcd_class.py
#  LCD Class based on work by Matt Hawkins
#  http://www.raspberrypi-spy.co.uk/
#
#  Wiring for the LCD screen is as per Matt's webpages:
#  http://www.raspberrypi-spy.co.uk/2012/07/16x2-lcd-module-control-using-python/
#  http://www.raspberrypi-spy.co.uk/2012/08/16x2-lcd-module-control-with-backlight-switch/
#
# Updated by Andy Fair:
#  - turned into class
#  - added backlight timer (backlight stays on for given time)
#  - integrated support for both 16x2 and 20x4 displays
#  - added custom char creation from Adafruit library
#
# TO DO:
#
#  Updated   : 10/07/2016
# --------------------------------------

# imports
useConfigParser = False
try:
    import configparser
except ImportError:
    import ConfigParser
    useConfigParser = True
import threading
import time
import RPi.GPIO as GPIO
from log_class import Log

# Define GPIO to LCD mapping
LCD_RS = 7
LCD_E = 8
LCD_D4 = 25
LCD_D5 = 24
LCD_D6 = 23
LCD_D7 = 18
LCD_ON = 12

# Timing constants
E_PULSE = 0.0015  # Pulse width of enable
E_DELAY = 0.0015  # Delay between writes
E_POSTCLEAR = 0.3  # Delay after clearing display

# Control for backlight
LCD_SETCGRAMADDR = 0x40

# Values from config file - default values only, should be overwritten by configparser
LCD_WIDTH = 16
LCD_HEIGHT = 2
BACKLIGHT_DURATION = 30

log = Log()

class Lcd:
    # Define LCD device constants
    LCD_CHR = True
    LCD_CMD = False

    LCD_LINE_1 = 0x80  # LCD RAM address for the 1st line
    LCD_LINE_2 = 0xC0  # LCD RAM address for the 2nd line
    LCD_LINE_3 = 0x94  # LCD RAM address for the 3rd line
    LCD_LINE_4 = 0xD4  # LCD RAM address for the 4th line

    LJUST = 1
    CENTRE = 2
    RJUST = 3

    WRITE_HAPPENING = False
    BACKLIGHT_TIMER = 0

    def __init__(self):
        global LCD_WIDTH, LCD_HEIGHT, BACKLIGHT_DURATION

        if (useConfigParser == True):
            cp = ConfigParser.ConfigParser()
        else:
            cp = configparser.ConfigParser()
        cp.read ("/home/pi/heating/heating.cfg")
        LCD_WIDTH = cp.getint('lcd', 'LCD_WIDTH')
        LCD_HEIGHT = cp.getint('lcd', 'LCD_HEIGHT')
        BACKLIGHT_DURATION = cp.getint('lcd', 'BACKLIGHT_DURATION')
        return

    def init(self):
        GPIO.setwarnings(False)  # Disable warnings
        GPIO.setmode(GPIO.BCM)  # Use BCM GPIO numbers
        GPIO.setup(LCD_E, GPIO.OUT)  # E
        GPIO.setup(LCD_RS, GPIO.OUT)  # RS
        GPIO.setup(LCD_D4, GPIO.OUT)  # DB4
        GPIO.setup(LCD_D5, GPIO.OUT)  # DB5
        GPIO.setup(LCD_D6, GPIO.OUT)  # DB6
        GPIO.setup(LCD_D7, GPIO.OUT)  # DB7
        GPIO.setup(LCD_ON, GPIO.OUT)  # Backlight enable
        self.initdisplay()

    # Initialise display
    def initdisplay(self):
        self.wait_for_write()

        # Send string to display
        self.start_write()
        self.lcd_backlight(False)  # Make sure backlight is turned off
        self.lcd_byte(0x33, self.LCD_CMD)  # 110011 Initialise
        self.lcd_byte(0x32, self.LCD_CMD)  # 110010 Initialise
        self.lcd_byte(0x06, self.LCD_CMD)  # 000110 Cursor move direction
        self.lcd_byte(0x0C, self.LCD_CMD)  # 001100 Display On,Cursor Off, Blink Off
        self.lcd_byte(0x28, self.LCD_CMD)  # 101000 Data length, number of lines, font size
        self.lcd_byte(0x01, self.LCD_CMD)  # 000001 Clear display
        time.sleep(E_POSTCLEAR)            # Allow display to settle before using
        self.stop_write()

    def lcd_on(self):
        if (self.BACKLIGHT_TIMER > 0):  # Button has already been pressed and is counting down
            self.BACKLIGHT_TIMER = BACKLIGHT_DURATION  # Reset timer back to 30. No need to reschedule timer as it already exists
        else:
            self.BACKLIGHT_TIMER = BACKLIGHT_DURATION
            t = threading.Timer(1.0, self.lcd_off)
            t.start()
            self.lcd_backlight(True)

    def lcd_off(self):
        self.BACKLIGHT_TIMER -= 1
        if (self.BACKLIGHT_TIMER > 0):
            t = threading.Timer(1.0, self.lcd_off)  # Keep counting for another second
            t.start()
        elif (self.BACKLIGHT_TIMER == 0):  # If -1 then lcd_override_off has been called and program terminated
            self.lcd_backlight(False)

    def lcd_override_off(self):
        self.BACKLIGHT_TIMER = -1
        self.lcd_backlight(False)

    def lcd_toggle_enable(self):
        # Toggle enable
        time.sleep(E_DELAY)
        GPIO.output(LCD_E, True)
        time.sleep(E_PULSE)
        GPIO.output(LCD_E, False)
        time.sleep(E_DELAY)

    def lcd_byte(self, bits, mode):
        # Send byte to data pins
        # bits = data
        # mode = True  for character
        #        False for command
        GPIO.output(LCD_RS, mode)  # RS

        # High bits
        GPIO.output(LCD_D4, False)
        GPIO.output(LCD_D5, False)
        GPIO.output(LCD_D6, False)
        GPIO.output(LCD_D7, False)
        if bits & 0x10 == 0x10:
            GPIO.output(LCD_D4, True)
        if bits & 0x20 == 0x20:
            GPIO.output(LCD_D5, True)
        if bits & 0x40 == 0x40:
            GPIO.output(LCD_D6, True)
        if bits & 0x80 == 0x80:
            GPIO.output(LCD_D7, True)

        # Toggle 'Enable' pin
        self.lcd_toggle_enable()

        # Low bits
        GPIO.output(LCD_D4, False)
        GPIO.output(LCD_D5, False)
        GPIO.output(LCD_D6, False)
        GPIO.output(LCD_D7, False)
        if bits & 0x01 == 0x01:
            GPIO.output(LCD_D4, True)
        if bits & 0x02 == 0x02:
            GPIO.output(LCD_D5, True)
        if bits & 0x04 == 0x04:
            GPIO.output(LCD_D6, True)
        if bits & 0x08 == 0x08:
            GPIO.output(LCD_D7, True)

        # Toggle 'Enable' pin
        self.lcd_toggle_enable()

    def lcd_string(self, message, line, style):
        self.wait_for_write()

        # Send string to display
        self.start_write()
        if (style == self.LJUST):
            message = message.ljust(LCD_WIDTH, " ")
        elif (style == self.CENTRE):
            message = message.center(LCD_WIDTH, " ")
        elif (style == self.RJUST):
            message.rjust(LCD_WIDTH, " ")

        lcd_line = 0x00
        if (line == 1):
            lcd_line = self.LCD_LINE_1
        elif (line == 2):
            lcd_line = self.LCD_LINE_2
        elif ((line == 3) and (LCD_HEIGHT == 4)):
            lcd_line = self.LCD_LINE_3
        elif ((line == 4) and (LCD_HEIGHT == 4)):
            lcd_line = self.LCD_LINE_4

        if (lcd_line != 0x00):
            self.lcd_byte(lcd_line, self.LCD_CMD)
            for i in range(LCD_WIDTH):
                self.lcd_byte(ord(message[i]), self.LCD_CHR)

        self.stop_write()

    def wait_for_write(self):
        # Check if another thread is writing to the LCD screen to stop corruption
        # Don't continue until the other thread has finished or until 10sec has passed
        write_check = 0
        while (self.WRITE_HAPPENING == True):
            write_check += 1
            if (write_check > 20):
                log.log_message("LCD write process took too long. Continuing, screen corruption may happen.", log.WARNING)
                self.WRITE_HAPPENING = False
            time.sleep(0.5)

    def start_write(self):
        self.WRITE_HAPPENING = True

    def stop_write(self):
        self.WRITE_HAPPENING = False

    def lcd_clear(self):
        self.lcd_byte(0x01, self.LCD_CMD)

    def lcd_backlight(self,flag):
        # Toggle backlight on-off-on
        GPIO.output(LCD_ON, flag)

    def read_backlight(self):
        if (self.BACKLIGHT_TIMER > 0):
            return True
        else:
            return False

    def get_LCDHEIGHT(self):
        return LCD_HEIGHT

    def get_LCDWIDTH(self):
        return LCD_WIDTH

    def create_char(self, location, pattern):
        # Fill one of the first 8 CGRAM locations with custom characters.
        # The location parameter should be between 0 and 7 and pattern should
        # provide an array of 8 bytes containing the pattern
        # To show your custom character use eg. lcd.message('\x01')

        location &= 0x7     # only positions 0-7 are allowed
        self.lcd_byte(LCD_SETCGRAMADDR | (location << 3), False)
        for i in range(8):
            self.lcd_byte(pattern[i], True)
