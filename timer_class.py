#!/usr/bin/python
# --------------------------------------
#
#  timer_class.py
#  Class to setup and manage timers
#
# TO DO:
#
#  Author    : Andy Fair
#  Updated   : 11/07/2016
# --------------------------------------

# imports
from datetime import datetime, timedelta
from operator import itemgetter
from ast import literal_eval
from sunrise_class import sun
import threading
import internet
from log_class import Log
import sys

useConfigParser = False
try:
    import configparser
except ImportError:
    import ConfigParser
    useConfigParser = True

# GLOBAL CONSTANTS:
CURRENT_LONG = -3.9632781
CURRENT_LAT = 51.8068358
LOCAL_SUN = 0
LOCAL_SUNRISE = 0
LOCAL_SUNSET = 0

ABSURD_FALSE = 666

INTERNET_COUNTDOWN = 12 # Number of hours that Internet has to be down before temp delta values of 0 are used

log = Log()

InternetTimer = None
InternetCountdown = INTERNET_COUNTDOWN


class Timer:
    # Define schedule constants
    HEAT = 1
    WATER = 2
    RESET = 3

    TURN_ON = 1
    TURN_OFF = 2
    CHANGE_MODE = 3

    RUN = 1
    ADV = 2

    HEAT_STATUS = TURN_OFF
    WTR_STATUS = TURN_OFF

    RESET_IN_PROCESS = False

    PLUS_ONE_LENGTH = None

    TMP_DELTA = [0, 0, 0]  # Morning, midday, evening

    # Structure of list: [On-time, off-time, WATER/HEATING, RUN/ADV]
    # Possible values for On-time/Off-time:
    #   Time: in 24hr clock
    #   DAWN: Calculates dawn with modifier based on forecast weather conditions
    #   DUSK: Calculates dusk with modifier based on forecast weather conditions
    #   HHhMMm: Duration in hours and minutes (NB: Durations are only allowed for off-times)
    #
    # SCHEDULE is the master schedule tuple. It shouldn't be changed.
    # SCHEDCOPY is the working schedule list. It can be changed, modified and added to
    # These are just placeholders - values will be read from config file:
    SCHEDULE = ()
    SCHEDCOPY = []

    def __init__(self):
        global LOCAL_SUN, LOCAL_SUNRISE, LOCAL_SUNSET
        global SCHEDCOPY, SCHEDULE, useConfigParser, PLUS_ONE_LENGTH

        if (useConfigParser == True):
            cp = ConfigParser.ConfigParser()
        else:
            cp = configparser.ConfigParser()
        cp.read("/home/pi/heating/heating.cfg")

        PLUS_ONE_LENGTH = self.convert_duration(cp.get("timer", "PLUS_ONE_TIMER"))

        summer_start_str = cp.get("timer", "SUMMER_START") + '/' + datetime.today().strftime('%Y')
        summer_start = datetime.strptime(summer_start_str, '%d/%m/%Y')

        winter_start_str = cp.get("timer", "WINTER_START") + '/' + datetime.today().strftime('%Y')
        winter_start = datetime.strptime(winter_start_str, '%d/%m/%Y')

        today_date = datetime.today()
        if ((today_date >= summer_start) and (today_date < winter_start)):
            sched_str = cp.get("timer", "SUMMER_SCHEDULE")
            log.log_message("Using Summer schedule", log.INFO)
        else:
            sched_str = cp.get("timer", "WINTER_SCHEDULE")
            log.log_message("Using Winter schedule", log.INFO)

        sched_str = sched_str.replace("WATER", str(self.WATER))
        sched_str = sched_str.replace("HEAT", str(self.HEAT))
        sched_str = sched_str.replace("RUN", str(self.RUN))
        sched_str = sched_str.replace("ADV", str(self.ADV))

        SCHEDULE = literal_eval(sched_str)  # Safer version
        SCHEDCOPY = []          # I don't know why Python needs this to happen again

        self.TMP_DELTA = internet.forecast_temp_delta()

        SCHEDCOPY.extend(self.parse_schedule(SCHEDULE))

    def isValidDay(self, when, datestr):
        testdate = datetime.strptime(datestr,'%d/%m/%y')
        dayname = testdate.strftime('%a').upper()
        when = when.upper()
        weekdays = ["MON", "TUE", "WED", "THU", "FRI"]
        weekends = ["SAT", "SUN"]

        if (when == "EVERY"):
            retval = True
        elif ((when == "WEEKDAY") and (dayname in weekdays)):
            retval = True
        elif ((when == "WEEKEND") and (dayname in weekends)):
            retval = True
        elif (when == dayname):
            retval = True
        else:
            retval = False

        return retval

    def parse_time(self, time_str, today_str, start_time=None):
        if (sys.version_info[0]==3):    # Check if we're running Python 2.x or 3.x
            stringtypes = str           # If we're running 3.x, all strings are Unicode
        else:
            stringtypes = basestring    # If we're running 2.x, basestring includes Unicode strings (str does not)

        if (not(isinstance(time_str, stringtypes))):
            log.log_message("Passed non-string value to parse_time function", log.ERROR)
            retval = False
        else:
            if ((time_str.find("h") >= 0 or time_str.find("m") >= 0) and (start_time != None)):
                retval = start_time + timedelta(minutes=self.convert_duration(time_str))
            elif (time_str == "DAWN"):
                dawndusktime = self.calc_dawn(self.TMP_DELTA[0]).strftime('%H:%M')
                retval = datetime.strptime(today_str + ' ' + dawndusktime, '%d/%m/%y %H:%M')
            elif (time_str == "DUSK"):
                dawndusktime = self.calc_dusk(self.TMP_DELTA[1], 0).strftime('%H:%M')
                retval = datetime.strptime(today_str + ' ' + dawndusktime, '%d/%m/%y %H:%M')
            else:
                retval = datetime.strptime(today_str + " " + time_str, '%d/%m/%y %H:%M')

        return retval

    def parse_schedule(self, schedlist):
        global LOCAL_SUN, LOCAL_SUNRISE, LOCAL_SUNSET
        global SCHEDCOPY

        retval = []
        if ((type(schedlist) is tuple) or (type(schedlist) is list) and (len(schedlist) > 0)):
            LOCAL_SUN = sun(lat=CURRENT_LAT, long=CURRENT_LONG)
            for num_days in range(0,3):     # Generate three days worth of schedule
                today_str = (datetime.today() + timedelta(days=num_days)).strftime('%d/%m/%y')
                LOCAL_SUNRISE = LOCAL_SUN.sunrise(datetime.strptime(today_str + " 09:00", '%d/%m/%y %H:%M'))
                LOCAL_SUNSET = LOCAL_SUN.sunset(datetime.strptime(today_str + " 09:00", '%d/%m/%y %H:%M'))
                for i in range(0, len(schedlist)):
                    if (self.isValidDay(schedlist[i][3], today_str)):
                        starttime = self.parse_time(schedlist[i][0], today_str)
                        endtime = self.parse_time(schedlist[i][1], today_str, starttime)
                        mode = schedlist[i][2]
                        status = self.RUN
    
                        retval.append([starttime, endtime, mode, status])
        else:
            log.log_message("parse_schedule function passed a non-list argument or zero-length list", log.ERROR)

        return retval

    def reset(self, *args):
        global SCHEDCOPY, SCHEDULE, InternetTimer, InternetCountdown

        HEATING_STATUS = args[0]
        WATER_STATUS = args[1]
        STATUS_OFF = args[2]
        # Don't actually reset ANYTHING until there is an Internet connection
        # OR we've waited 12hrs without a connection and use default temp delta values of 0
        if (internet.is_connected() or (InternetCountdown < 1)):
            InternetTimer = None
            InternetCountdown = INTERNET_COUNTDOWN
            self.RESET_IN_PROCESS = True    # Stop timer check in main loop from accessing schedule while it's being reset
            # Reset two main variables, just in case something dodgy is going on.
            SCHEDULE = ()
            SCHEDCOPY = []
            self.__init__()
            if (HEATING_STATUS == STATUS_OFF):
                log.log_message("Removing heating timers", log.DEBUG)
                self.remove_category(self.HEAT)
            if (WATER_STATUS == STATUS_OFF):
                log.log_message("Removing water timers", log.DEBUG)
                self.remove_category(self.WATER)
            log.log_message("Schedule reset", log.INFO)
            self.print_schedule()
            self.RESET_IN_PROCESS = False
        else:
            # Countdown so that we don't get stuck in an endless loop if Internet is down for a long time
            InternetCountdown -= 1
            logmsg = "No Internet connection, trying again in one hour. " + str(InternetCountdown) + " attempts left"
            log.log_message(logmsg, log.WARNING)

            # No Internet connection, so schedule call back for 1 hour (3600 seconds)
            InternetTimer = threading.Timer(3600.0, self.reset, [HEATING_STATUS, WATER_STATUS, STATUS_OFF])
            InternetTimer.start()

    def getRESET(self):
        return self.RESET_IN_PROCESS

    def reset_one(self, mode):
        global LOCAL_SUN, LOCAL_SUNRISE, LOCAL_SUNSET
        global SCHEDCOPY, SCHEDULE

        self.remove_category(mode)

        # Now we need to re-add the original SCHEDULE settings...but first we need to filter them
        # New improved Tuple-to-list version:
        FILT_SCHED = [list(x) for x in SCHEDULE if x[2] == mode]
        SCHEDCOPY.extend(self.parse_schedule(FILT_SCHED))
        SCHEDCOPY.sort(key=itemgetter(0))  # Sort on start times
        self.clean_schedule()  # Tidy up schedule

    def get_schedcopy(self):
        global SCHEDCOPY
        return SCHEDCOPY

    def calc_dawn(self, tmpdelta):
        today_sunrise = LOCAL_SUNRISE
        nine_thirty = datetime.strptime(today_sunrise.strftime('%d/%m/%y') + " 09:30", '%d/%m/%y %H:%M')
        nine_am = datetime.strptime(today_sunrise.strftime('%d/%m/%y') + " 09:00", '%d/%m/%y %H:%M')
        time_diff = 7200 / ((nine_thirty - today_sunrise).seconds / 60)

        calc_time = nine_thirty - timedelta(minutes=time_diff) - timedelta(minutes=(5 * tmpdelta))
        if (calc_time > nine_am):
            calc_time = nine_am         # Makes sure that boiler always comes on no later than 9am

        return calc_time

    def calc_dusk(self, tmpdelta, start_or_end=0):
        if (start_or_end == 0):
            if (tmpdelta > 0):
                calc_time = LOCAL_SUNSET + timedelta(minutes=(5 * tmpdelta))
            else:
                # Yes, it's still a plus, because tmpdelta value is negative
                calc_time = LOCAL_SUNSET + timedelta(minutes=(10 * tmpdelta))
        else:
            if (tmpdelta > 0):
                calc_time = LOCAL_SUNSET - timedelta(minutes=(5 * tmpdelta))
            else:
                # Yes, it's still a minus, because tmpdelta value is negative, so actually adding time to the end
                calc_time = LOCAL_SUNSET - timedelta(minutes=(10 * tmpdelta))

        return calc_time

    def convert_duration(self, dur_str):
        time_str = ""
        duration = "".join(dur_str.split()).lower()  # Remove any stray spaces and convert to lower case

        find_h = duration.find("h")
        find_m = duration.find("m")

        if (find_h > 0 and find_m > 0):  # XXhYYm
            time_str = "%Hh%Mm"
        elif (find_h > 0 and find_m == -1):  # XXh..
            if ((len(duration) - 1) > find_h):  # XXhYY
                time_str = "%Hh%M"
            else:  # XXh
                time_str = "%Hh"
        elif (find_h == -1 and find_m > 0):  # YYm
            time_str = "%Mm"

        if (len(time_str) > 0):
            timeval = datetime.strptime(duration, time_str)
            retval = (timeval.hour * 60) + timeval.minute
        else:
            retval = 0

        return retval

    def check_schedule(self):
        global SCHEDCOPY, HEAT_STATUS, WTR_STATUS

        current_time = datetime.strptime(datetime.now().strftime("%H:%M %d/%m/%y"), "%H:%M %d/%m/%y")
        HEAT_STATUS = self.TURN_OFF
        WTR_STATUS = self.TURN_OFF

        for settings in SCHEDCOPY:
            if ((current_time >= settings[0]) and (current_time < settings[1])):
                # Note to self: these IFs need to be separated, otherwise ADV state will cause fall-through to error msg
                if (settings[2] == self.HEAT):
                    if (settings[3] == self.RUN):
                        HEAT_STATUS = self.TURN_ON
                elif (settings[2] == self.WATER):
                    if (settings[3] == self.RUN):
                        WTR_STATUS = self.TURN_ON
                else:
                    log.log_message("Mismatch value in timer list", log.ERROR)
            elif (current_time == settings[1]):     # We're at the end time
                if (settings[2] == self.HEAT):
                    if (settings[3] == self.ADV):
                        HEAT_STATUS = self.CHANGE_MODE
                elif (settings[2] == self.WATER):
                    if (settings[3] == self.ADV):
                        WTR_STATUS = self.CHANGE_MODE
                else:
                    log.log_message("Mismatch value in timer list", log.ERROR)

        return HEAT_STATUS, WTR_STATUS  # Leave turning relays on/off down to main program

    def add_hour(self, mode):  # Add 1 hour slot to schedule
        global SCHEDCOPY, PLUS_ONE_LENGTH

        current_time = datetime.strptime(datetime.now().strftime("%H:%M %d/%m/%y"), "%H:%M %d/%m/%y")
        # off_time = current_time + timedelta(hours=1)
        off_time = current_time + timedelta(minutes=PLUS_ONE_LENGTH)
        index, on_off = self.find_next(mode)

        if (on_off == self.TURN_ON):
            # If next event is to turn on, then we can just add a new event:
            SCHEDCOPY.append([current_time, off_time, mode, self.RUN])
        else:
            # If next event is to turn off, we're in the middle of an event, so we need to change the end time
            SCHEDCOPY[index][1] = off_time

    def cancel_add_hour(self, mode):  # Cancel the 1 hour slot
        self.reset_one(mode)

    def add_adv(self, mode):  # Advance next setting
        global SCHEDCOPY

        retval = 0
        current_time = datetime.strptime(datetime.now().strftime("%H:%M %d/%m/%y"), "%H:%M %d/%m/%y")
        index, on_off = self.find_next(mode)

        if (index != ABSURD_FALSE):
            if (on_off == self.TURN_ON):
                SCHEDCOPY[index][0] = current_time
                retval = self.TURN_ON
            elif (on_off == self.TURN_OFF):
                # Old way:
                # del SCHEDCOPY[index]
                SCHEDCOPY[index][3] = self.ADV
                retval = self.TURN_OFF
        else:
            # Nothing found - we're after all schedules - so create one from now till midnight
            # Note: with the new 3-day advance schedule, this should not happen...but just in case:
            off_time = datetime.strptime(current_time.strftime('%d/%m/%y') + " 23:59", '%d/%m/%y %H:%M')
            SCHEDCOPY.append([current_time, off_time, mode, self.RUN])
            retval = ABSURD_FALSE

        return retval

    def cancel_adv(self, mode):  # Cancel advance slot
        self.reset_one(mode)

    def find_next(self, mode):  # Find next timeslot due to run
        global SCHEDCOPY

        current_time = datetime.strptime(datetime.now().strftime("%H:%M %d/%m/%y"), "%H:%M %d/%m/%y")
        on_time = off_time = current_time
        on_off = ABSURD_FALSE
        status = ABSURD_FALSE
        index = ABSURD_FALSE
        found_index = ABSURD_FALSE

        newsched = [y for y in SCHEDCOPY if y[2] == mode]

        if (len(newsched) > 0):
            newsched.sort(key=itemgetter(0))  # Sort on start times

            for i in range(0, len(newsched)):
                if ((current_time >= newsched[i][0]) and (current_time < newsched[i][1]) and (newsched[i][2] == mode) and (index == ABSURD_FALSE)):
                    # In the middle of a timer schedule: next event is OFF @ newsched[i][1]
                    on_time = newsched[i][0]
                    off_time = newsched[i][1]
                    status = newsched[i][3]
                    on_off = self.TURN_OFF
                    index = i
                if ((i > 0) and (current_time < newsched[i][0]) and (current_time >= newsched[i - 1][1]) and (newsched[i][2] == mode) and (index == ABSURD_FALSE)):
                    # In between schedules: next event is ON @ newsched[i][0]
                    on_time = newsched[i][0]
                    off_time = newsched[i][1]
                    status = newsched[i][3]
                    on_off = self.TURN_ON
                    index = i
                if ((i == 0) and (current_time < newsched[i][0]) and (newsched[i][2] == mode) and (index == ABSURD_FALSE)):
                    # Before first event: next event is ON @ newsched[i][0]
                    on_time = newsched[i][0]
                    off_time = newsched[i][1]
                    status = newsched[i][3]
                    on_off = self.TURN_ON
                    index = i

            if (on_off != ABSURD_FALSE):
                found_index = SCHEDCOPY.index([on_time, off_time, mode, status])
            else:
                log.log_message(newsched, log.ERROR)
                if (mode == self.WATER):
                    log.log_message("Can't find any more occurrences for WATER", log.ERROR)
                else:
                    log.log_message("Can't find any more occurrences for HEAT", log.ERROR)
        return found_index, on_off

    def find_next_time(self, mode):
        global SCHEDCOPY

        retstr = "AHA!"
        index, on_off = self.find_next(mode)
        if (index != ABSURD_FALSE):
            if (on_off == self.TURN_ON):
                retstr = "ON at " + SCHEDCOPY[index][0].strftime('%H:%M')
            else:
                retstr = "OFF at " + SCHEDCOPY[index][1].strftime('%H:%M')
        else:
            retstr = "OFF"
        return retstr

    def clean_schedule(self):  # Remove past events from SCHEDCOPY
        global SCHEDCOPY

        to_delete = []
        current_time = datetime.strptime(datetime.now().strftime("%H:%M %d/%m/%y"), "%H:%M %d/%m/%y")

        for i in range(len(SCHEDCOPY)):
            if ((SCHEDCOPY[i][0] < current_time) and (SCHEDCOPY[i][1] < current_time)):
                # If both start and stop times are in the past
                to_delete.append(i)

        if (len(to_delete) > 0):
            # Sort list descending so we delete from the end in
            # (if we delete from the start up, indices keep changing)
            to_delete.sort(reverse=True)
            for index in to_delete:
                del SCHEDCOPY[index]

    def remove_category(self, mode):  # Remove all of one type of event from list
        global SCHEDCOPY

        to_delete = []
        for i in range(len(SCHEDCOPY)):
            if (SCHEDCOPY[i][2] == mode):
                to_delete.append(i)

        if (len(to_delete) > 0):
            # Sort list descending so we delete from the end in
            # (if we delete from the start up, indices keep changing)
            to_delete.sort(reverse=True)
            for index in to_delete:
                del SCHEDCOPY[index]

    def print_schedule(self):
        global SCHEDCOPY

        log.log_message("|=======|===========|===========|", log.INFO)
        log.log_message("| WHAT? |  ON TIME  | OFF TIME  |", log.INFO)
        log.log_message("|=======|===========|===========|", log.INFO)
        for i in range(0, len(SCHEDCOPY)):
            if (SCHEDCOPY[i][2] == self.WATER):
                what = "WATER"
            else:
                what = "HEAT "

            logstr = "| " + what + " | " + SCHEDCOPY[i][0].strftime('%a %H:%M') + " | " + SCHEDCOPY[i][1].strftime('%a %H:%M') + " |"
            log.log_message(logstr, log.INFO)
            log.log_message("|-------|-----------|-----------|", log.INFO)

    def cancel_timer(self):
        global InternetTimer

        if (InternetTimer != None):
            InternetTimer.cancel()
