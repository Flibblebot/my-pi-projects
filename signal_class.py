#!/usr/bin/python
# --------------------------------------
#
#  signal_class.py
#  Class to manage OS signals, especially the TERM signal
#
#  Author : Andy Fair
#  Date   : 23/05/2016
# --------------------------------------

# imports
import signal

class SignalCheck:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    self.kill_now = True

