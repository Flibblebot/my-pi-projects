#!/usr/bin/python
# --------------------------------------
#
#  internet.py
#  Functions to check for Internet availability and use Internet (Met Office) weather forecasts to
#  calculate differences between mean local temp and forecast local temp
#
# TO DO:
#  - Reschedule calculation if no Internet available - DONE, handled by timer_class
#  - Put relevant variables into cfg file - DONE
#
#  Author    : Andy Fair
#  Updated   : 29/06/2016
# --------------------------------------

import datetime
import socket
from ast import literal_eval
import metoffer
from log_class import Log

useConfigParser = False
try:
    import configparser
except ImportError:
    import ConfigParser
    useConfigParser = True

MIDDAY = 1
MORNING = 2
EVENING = 3

ABSURD_VALUE = 666

log = Log()

def is_connected():
    REMOTE_SERVER = "www.google.com"
    try:
        # see if we can resolve the host name -- tells us if there is a DNS listening
        host = socket.gethostbyname(REMOTE_SERVER)
        # connect to the host -- tells us if the host is actually reachable
        s = socket.create_connection((host, 443), 2)
        return True
    except:
        pass
        return False

def forecast_temp_delta():
    if (useConfigParser == True):
        cp = ConfigParser.ConfigParser()
    else:
        cp = configparser.ConfigParser()
    cp.read("/home/pi/heating/heating.cfg")
    avetemp_str = cp.get("internet", "AVE_TEMPS")
    avetemps = literal_eval(avetemp_str)  # Safer version
    sdtemp_str =  cp.get("internet", "SD_TEMPS")
    sdtemps = literal_eval(sdtemp_str)
    met_offer_api = cp.get("internet", "MET_OFFER_API_KEY")
    current_long = cp.getfloat("internet", "LONGITUDE")
    current_lat = cp.getfloat("internet", "LATITUDE")

#    avetemps = [4.5, 4.4, 5.8, 8.1, 10.8, 13.6, 15.2, 15, 13.2, 10.5, 7.3, 4.9]
#    sdtemps = [1.4, 1.1, 1.2, 1.3, 0.7, 0.7, 1.1, 0.7, 0.8, 1.3, 1.2, 2.0]
#    met_offer_api = '684c5fd0-0238-4f0a-8965-1dcc0f5434e4'
#    current_long = -3.9632781
#    current_lat = 51.8068358
    midday_temp = morning_temp = evening_temp = ABSURD_VALUE

    if (is_connected()):
        met_forecast = metoffer.MetOffer(met_offer_api).nearest_loc_forecast(current_lat, current_long, metoffer.THREE_HOURLY)
        current_mo_forecast = metoffer.parse_val(met_forecast)

        today_morning = datetime.datetime.combine(datetime.date.today(), datetime.time(hour=9, minute=00))
        today_midday = datetime.datetime.combine(datetime.date.today(), datetime.time(hour=15, minute=00))
        today_evening = datetime.datetime.combine(datetime.date.today(), datetime.time(hour=21, minute=00))

        for i in current_mo_forecast.data:
            if (i["timestamp"][0] == today_midday):
                midday_temp = i["Feels Like Temperature"][0]
            if (i["timestamp"][0] == today_morning):
                morning_temp = i["Feels Like Temperature"][0]
            if (i["timestamp"][0] == today_evening):
                evening_temp = i["Feels Like Temperature"][0]

    this_month = datetime.datetime.now().month
    min_ave_temp = round(avetemps[this_month - 1] - sdtemps[this_month - 1], 2)
    max_ave_temp = round(avetemps[this_month - 1] + sdtemps[this_month - 1], 2)
    midtemp_delta = amtemp_delta = evetemp_delta = 0
    
    if (midday_temp != ABSURD_VALUE):
        if (midday_temp > max_ave_temp):
            midtemp_delta = midday_temp - max_ave_temp
        else:
            midtemp_delta = -(min_ave_temp - midday_temp)

    if (morning_temp != ABSURD_VALUE):
        if (morning_temp > max_ave_temp):
            amtemp_delta = morning_temp - max_ave_temp
        else:
            amtemp_delta = -(min_ave_temp - morning_temp)

    if (evening_temp != ABSURD_VALUE):
        if (evening_temp > max_ave_temp):
            evetemp_delta = evening_temp - max_ave_temp
        else:
            evetemp_delta = -(min_ave_temp - evening_temp)

    logmsg = "Morning delta: " + str(amtemp_delta)
    log.log_message(logmsg, log.DEBUG)
    logmsg = "Midday delta: " + str(midtemp_delta)
    log.log_message(logmsg, log.DEBUG)
    logmsg = "Evening delta: " + str(evetemp_delta)
    log.log_message(logmsg, log.DEBUG)

    return [amtemp_delta, midtemp_delta, evetemp_delta]
