#!/usr/bin/python
# --------------------------------------
#
#  temp_class.py
#  Class to read temperature from DS18S20 temp sensor
#
#  This class assumes that w1-gpio and w1-therm kernel modules have already been loaded
#  e.g. by add the two modprobe lines to /etc/rc.local
#
# TO DO:
#  - Avoid modprobe error messages
#
#  Author    : Andy Fair
#  Updated   : 16/06/2016
# --------------------------------------

# imports
import glob
import time
import os
import inspect

from log_class import Log
useConfigParser = False
try:
    import configparser
except ImportError:
    import ConfigParser
    useConfigParser = True

NUMBER_OF_READINGS = 20
MIN_TEMP = 29

log = Log()

class Temperature:
    device_file = ""
    temps_list = []  # List for previous readings

    def __init__(self):
        global NUMBER_OF_READINGS, MIN_TEMP

        therm_types = ['42', '3B', '28', '22', '10']    # List of sensor types supported by the w1 kernel modules
        base_dir = '/sys/bus/w1/devices/'

        # Open config file and read values from the file:
        if (useConfigParser == True):
            cp = ConfigParser.ConfigParser()
        else:
            cp = configparser.ConfigParser()

        filename = inspect.getframeinfo(inspect.currentframe()).filename
        path = os.path.dirname(os.path.abspath(filename))
        config_file = path + '/heating.cfg'

        cp.read(config_file)

        NUMBER_OF_READINGS = cp.getint("temp", "NUMBER_OF_READINGS")
        MIN_TEMP = cp.getint("temp", "MIN_TEMP")

        for therm_dir in therm_types:
            root_folder = glob.glob(base_dir + therm_dir + '*')
            if (len(root_folder) > 0):
                self.device_file = root_folder[0] + '/w1_slave' # Assumes we're only using 1 sensor

        if (len(self.device_file) == 0):
            log.log_message("Can't find 1-wire temperature device", log.ERROR)

        self.temps_list = [0]*NUMBER_OF_READINGS

    def read_temp_raw(self):
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp(self):
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            temp_c = float(temp_string) / 1000.0
            return temp_c

    def shift(self, seq, n):
        n %= len(seq)
        return seq[n:] + seq[:n]

    def update_temps(self):
        self.temps_list = self.shift(self.temps_list,1)   # Shift list so first result becomes last
        self.temps_list[NUMBER_OF_READINGS-1] = self.read_temp()        # Replace the last value with a new reading

        return sum(self.temps_list)                  # Return sum of the last 10 readings

    def too_cold(self):
        if ((self.update_temps() <= (MIN_TEMP * NUMBER_OF_READINGS)) and (self.temps_list.count(0) == 0)):
            return True
        else:
            return False
