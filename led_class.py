#!/usr/bin/python
# --------------------------------------
#
#  led_class.py
#  Class to control 3xLEDs
#
#  Author : Andy Fair
#  Date   : 12/05/2016
# --------------------------------------

# imports
import RPi.GPIO as GPIO

# Define GPIO to LED mapping
LED_ON = GPIO.HIGH
LED_OFF = GPIO.LOW

class Led:
    PWR_LED = 17
    HTG_LED = 27
    WTR_LED = 22

    def __init__(self):
        return

    def init(self):
        GPIO.setup(self.PWR_LED, GPIO.OUT)  # Power LED
        GPIO.setup(self.HTG_LED, GPIO.OUT)  # Heating LED
        GPIO.setup(self.WTR_LED, GPIO.OUT)  # Hot Water LED

        # Make sure all LEDs are turned off initially:
        GPIO.output(self.PWR_LED, LED_OFF)
        GPIO.output(self.HTG_LED, LED_OFF)
        GPIO.output(self.WTR_LED, LED_OFF)

    def led_on(self, led_pin):
        GPIO.output(led_pin,LED_ON)

    def led_off(self, led_pin):
        GPIO.output(led_pin,LED_OFF)
