#!/usr/bin/python
# --------------------------------------
#
#  relay_class.py
#  Class to control 4 Relay Module
#
#  Author : Andy Fair
#  Date   : 12/05/2016
# --------------------------------------

# imports
import RPi.GPIO as GPIO

# Define GPIO to Relay mapping
RELAY_ON = GPIO.LOW
RELAY_OFF = GPIO.HIGH

class Relay:
    PWR_RELAY = 16
    HTG_RELAY = 20
    WTR_RELAY = 21

    def __init__(self):
        return

    def init(self):
        # Initialise and turn all relays off
        GPIO.setup(self.PWR_RELAY, GPIO.OUT)  # Power LED
        GPIO.output(self.PWR_RELAY, RELAY_OFF)
        GPIO.setup(self.HTG_RELAY, GPIO.OUT)  # Heating LED
        GPIO.output(self.HTG_RELAY, RELAY_OFF)
        GPIO.setup(self.WTR_RELAY, GPIO.OUT)  # Hot Water LED
        GPIO.output(self.WTR_RELAY, RELAY_OFF)

    def relay_on(self, relay_pin):
        GPIO.output(relay_pin, RELAY_ON)

    def relay_off(self, relay_pin):
        GPIO.output(relay_pin, RELAY_OFF)
