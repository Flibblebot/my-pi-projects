#!/usr/bin/python
# --------------------------------------
#
#  main.py
#  Main program loop and handlers
#
#  This program relies on two external Python modules to be installed:
#  MetOffer: Wrapper for British Met Office API to receive forecasts based on location
#          : https://pypi.python.org/pypi/MetOffer
#  pytz    : World timezone definitions & conversions
#          : https://pypi.python.org/pypi/pytz
#
# TO DO:
#
#  Author    : Andy Fair
#  Updated   : 11/07/2016
# --------------------------------------
# System imports
import RPi.GPIO as GPIO
import time
from datetime import datetime, date
import threading
import inspect
import os

# Project imports
from lcd_class import Lcd
from led_class import Led
from relay_class import Relay
from timer_class import Timer
from signal_class import SignalCheck
from temp_class import Temperature
from log_class import Log

useConfigParser = False
try:
    import configparser
except ImportError:
    import ConfigParser
    useConfigParser = True

# Define push button GPIO and status
HTG_BUTTON = 26
HTG_BUTTON_STATUS = 1
HTG_ADV_STATUS = 0
WTR_BUTTON = 19
WTR_BUTTON_STATUS = 1
WTR_ADV_STATUS = 0

# Define status codes
STATUS_RUN = 1
STATUS_PLUSONE = 2
STATUS_ADV = 3
STATUS_OFF = 4

# Set initial states to off
HEATING_STATUS = STATUS_RUN
WATER_STATUS = STATUS_RUN

# Do we want to check (and act) on water temperature?
# Temporary setting - over-ridden by config file setting
CHECK_WATER = False

# Threading control
TimerStop = False
ClockTimer = None

# Set up classes
log = Log()
log.log_message("Starting schedule", log.INFO)
lcd = Lcd()
led = Led()
relay = Relay()
signal = SignalCheck()
water_temp = Temperature()
timer = Timer()

def main():
    global HEATING_STATUS, WATER_STATUS
    global HTG_BUTTON_STATUS, WTR_BUTTON_STATUS
    global HTG_ADV_STATUS, WTR_ADV_STATUS
    global CHECK_WATER
    global ClockTimer

    # Open config file and read values from the file:
    if (useConfigParser == True):
        cp = ConfigParser.ConfigParser()
    else:
        cp = configparser.ConfigParser()

    filename = inspect.getframeinfo(inspect.currentframe()).filename
    path = os.path.dirname(os.path.abspath(filename))
    config_file = path + '/heating.cfg'

    logstr = "Using log file at" + config_file
    log.log_message(logstr, log.DEBUG)

    cp.read(config_file)

    CHECK_WATER = cp.getboolean("temp", "CHECK_WATER")

    heat = water = 0
    old_heat = old_water = 0
    old_date = today_date = date.today()
    htg_already_off = wtr_already_off = 0
    htg_status = heat_str = ""
    wtr_status = water_str = ""

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)  # Use BCM GPIO numbers

    # Initialise display, LEDs, relays and buttons
    lcd.init()
    led.init()
    relay.init()
    button_init()
    lcd.create_char(0,[0x2,0x1,0xb,0x1b,0x1f,0xf,0x6,0x0])      # Create custom flame character
    lcd.lcd_on()

    # Display time on screen now so we don't have to wait a minute for the first timer to trigger
    lcd.lcd_string(datetime.now().strftime('%d/%m/%y %H:%M'), 1, lcd.CENTRE)
    # Setup timer to update clock every 30 seconds
    ClockTimer = threading.Timer(30.0, ClockDisplay)
    ClockTimer.start()

    relay.relay_on(relay.PWR_RELAY)     # Turn main power relay on
    led.led_on(led.PWR_LED)             # Turn power LED on

    timer.print_schedule()
    print_next_action(timer.HEAT)
    print_next_action(timer.WATER)

    while (signal.kill_now != True):
        old_heat = heat
        old_water = water
        old_date = today_date

        today_date = date.today()
        if (old_date != today_date):    # We've crossed over into tomorrow
            lcd.initdisplay()           # Reset LCD display to (hopefully) reduce occurrences of screen blanking
            htg_status = ""             # Invalidate variables so screen is fully updated next time round
            wtr_status = ""
            timer.reset(HEATING_STATUS, WATER_STATUS, STATUS_OFF)

        if (timer.getRESET() == False):         # Only check schedule if it's not being reset
            heat, water = timer.check_schedule()

        if (HEATING_STATUS != STATUS_OFF):
            htg_already_off = 0
            if (old_heat != heat):
                if (heat == timer.TURN_ON):     # We're moving from an OFF to an ON state
# Times when this will happen:
# :: At normal start of timer event - RUN is already status
# :: At start of +1H event - +1H is already status
# :: In between events when ADV is set - ADV
                    heating_on()
                    logstr = "Heating turned on at " + datetime.now().strftime("%H:%M")
                    log.log_message(logstr, log.INFO)
                    print_next_action(timer.HEAT)
                    if (HTG_ADV_STATUS == timer.TURN_OFF):
                        HEATING_STATUS = STATUS_RUN
                        HTG_BUTTON_STATUS = 1
                        HTG_ADV_STATUS = 0
                if (heat == timer.TURN_OFF):   # We're moving from an ON to an OFF state
# Times when this will happen:
# :: At normal end of timer event - RUN is already status, but doesn't hurt to set it to RUN (e.g. end of ADV timer)
# :: At end of +1H event - set status to RUN
# :: In middle of timer event when ADV is set - set status to ADV
                    heating_off()
                    logstr = "Heating turned off at " + datetime.now().strftime("%H:%M")
                    log.log_message(logstr, log.INFO)
                    print_next_action(timer.HEAT)
                    if (HTG_BUTTON_STATUS != STATUS_ADV):   # We've come to the end of +1H, or moving from OFF to RUN
                        HEATING_STATUS = STATUS_RUN
                        HTG_BUTTON_STATUS = 1
                    else:
                        # Need to work out why OFF state was triggered:
                        # Was it because ADV caused the OFF state (ADV activated in middle of timer event)
                        # or because we've reached the end of an ADV timer state?
                        #log.log_message("I have no idea how to handle this", log.ERROR)
                        if (HTG_ADV_STATUS == timer.TURN_ON):
                            HEATING_STATUS = STATUS_RUN
                            HTG_BUTTON_STATUS = 1
                            HTG_ADV_STATUS = 0
                if (heat == timer.CHANGE_MODE):
                    HEATING_STATUS = STATUS_RUN
                    HTG_BUTTON_STATUS = 1
        else:
            if (not htg_already_off):
                heating_off()
                logstr = "Heating turned off at " + datetime.now().strftime("%H:%M")
                log.log_message(logstr, log.INFO)
                print_next_action(timer.HEAT)
                htg_already_off = 1
                heat = 0

        if (WATER_STATUS != STATUS_OFF):
            wtr_already_off = 0
            if (old_water != water):
                if (water == timer.TURN_ON):    # We're moving from an OFF to an ON state
                    water_on()
                    logstr = "Water turned on at " + datetime.now().strftime("%H:%M")
                    log.log_message(logstr, log.INFO)
                    print_next_action(timer.WATER)
                    if (WTR_ADV_STATUS == timer.TURN_OFF):
                        WATER_STATUS = STATUS_RUN
                        WTR_BUTTON_STATUS = 1
                        WTR_ADV_STATUS = 0
                if (water == timer.TURN_OFF):   # We're moving from an ON to an OFF state
                    water_off()
                    logstr = "Water turned off at " + datetime.now().strftime("%H:%M")
                    log.log_message(logstr, log.INFO)
                    print_next_action(timer.WATER)
                    if (WTR_BUTTON_STATUS != STATUS_ADV):   # We've come to the end of +1H, or moving from OFF to RUN
                        WTR_BUTTON_STATUS = 1
                        WATER_STATUS = STATUS_RUN
                    else:
                        if (WTR_ADV_STATUS == timer.TURN_ON):
                            WATER_STATUS = STATUS_RUN
                            WTR_BUTTON_STATUS = 1
                            WTR_ADV_STATUS = 0
                    if (water == timer.CHANGE_MODE):
                        WATER_STATUS = STATUS_RUN
                        WTR_BUTTON_STATUS = 1
        else:
            if (not wtr_already_off):
                water_off()
                logstr = "Water turned off at " + datetime.now().strftime("%H:%M")
                log.log_message(logstr, log.INFO)
                print_next_action(timer.WATER)
                wtr_already_off = 1
                water = 0

        old_htg_status = htg_status
        old_water_status = wtr_status
        if (HEATING_STATUS == STATUS_RUN):
            htg_status="RUN"
        elif (HEATING_STATUS == STATUS_OFF):
            htg_status="OFF"
        elif (HEATING_STATUS == STATUS_ADV):
            htg_status="ADV"
        elif (HEATING_STATUS == STATUS_PLUSONE):
            htg_status="+1H"
        else:
            htg_status="ERR"

        if (WATER_STATUS == STATUS_RUN):
            wtr_status="RUN"
        elif (WATER_STATUS == STATUS_OFF):
            wtr_status="OFF"
        elif (WATER_STATUS == STATUS_ADV):
            wtr_status="ADV"
        elif (WATER_STATUS == STATUS_PLUSONE):
            wtr_status="+1H"
        else:
            wtr_status="ERR"

        if ((old_htg_status != htg_status) or (old_water_status != wtr_status)):
            # Pad status string to heating is left justified, water is right justified on same line
            # Takes into account differing LCD screen widths
            htg_status_string = "Htg:" + htg_status
            wtr_status_string = "Wtr:" + wtr_status
            status_string_padding = (lcd.get_LCDWIDTH() - len(htg_status_string) - len(wtr_status_string)) * " "
            status_string = htg_status_string + status_string_padding + wtr_status_string
            lcd.lcd_string(status_string, 2, lcd.LJUST)

        if (lcd.get_LCDHEIGHT() > 2):
            if (timer.getRESET() == False):     # If timer is being reset, don't check schedule
                old_heat_str = heat_str
                old_water_str = water_str
                if (heat == timer.TURN_ON):
                    htg_prefix = '\x00'
                else:
                    htg_prefix = ' '

                if (water == timer.TURN_ON):
                    wtr_prefix = '\x00'
                else:
                    wtr_prefix = ' '

                heat_str = htg_prefix + "HEAT:  " + timer.find_next_time(timer.HEAT)
                water_str = wtr_prefix + "WATER: " + timer.find_next_time(timer.WATER)

                if((old_heat_str != heat_str) or (old_water_str != water_str)):
                    lcd.lcd_string(heat_str, 3, lcd.LJUST)
                    lcd.lcd_string(water_str, 4, lcd.LJUST)

        time.sleep(1)

def ClockDisplay():
    global TimerStop, ClockTimer

    if (TimerStop == False):
        ClockTimer = threading.Timer(30.0, ClockDisplay)
        ClockTimer.start()
        lcd.lcd_string(datetime.now().strftime('%d/%m/%y %H:%M'), 1, lcd.CENTRE)
        if (CHECK_WATER == True):
            if (timer.getRESET() == False):  # Only check schedule if it's not being reset
                heat, water = timer.check_schedule()
            else:
                heat, water = 0,0

            if ((water_temp.too_cold()) and (water == timer.TURN_OFF)): # If temp is too low AND water is turned off
                log.log_message("Water temp too cold. Turning water on for 1hr", log.INFO)
                timer.reset_one(timer.WATER)
                timer.add_hour(timer.WATER)

def button_init():
    GPIO.setup(HTG_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(HTG_BUTTON, GPIO.FALLING, callback=htg_button_press, bouncetime=300)
    GPIO.setup(WTR_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(WTR_BUTTON, GPIO.FALLING, callback=wtr_button_press, bouncetime=300)

def debounce_input(pin, pull_up_down):
# debounce_input reads the specified GPIO pin and returns the first state (0 or 1) that we read three times in a row
# Taken from http://raspberrypi.stackexchange.com/questions/44720/false-positive-press-of-a-button
    TRIES = 12
    bit, i, ones, zeroes = 0, 0, 0, 0

    if (pull_up_down == GPIO.PUD_UP):
        BIT_CHECK = 0       # If pin is pulled up, pin status of zero is button down
    else:
        BIT_CHECK = 1       # If pin is pulled low, pin status of one is button down

    while i < TRIES:
        bit = GPIO.input(pin)
        if (bit == BIT_CHECK):
            ones += 1
            zeroes = 0
        else:
            zeroes += 1
            ones = 0

        i += 1
        if (ones >= 3):
            return True
        if (zeroes >= 3):
            return False
        time.sleep(0.1)  # wait a bit

    # Haven't been able to accurately determine state.
    log.log_message("Error debouncing button press", log.DEBUG)
    return False

def htg_button_press(channel):
    global HTG_BUTTON_STATUS, HEATING_STATUS
    global timer, HTG_ADV_STATUS
    status_str = ""

    if (debounce_input(channel, GPIO.PUD_UP)):
        if (lcd.read_backlight()):  # If True then backlight already on
            HTG_BUTTON_STATUS += 1
            if (HTG_BUTTON_STATUS > 5):
                HTG_BUTTON_STATUS = 2   # Loop back round again

            if (HTG_BUTTON_STATUS == 2):
                HEATING_STATUS = STATUS_PLUSONE
                timer.reset_one(timer.HEAT)
                timer.add_hour(timer.HEAT)
                status_str = "+1H"
            elif (HTG_BUTTON_STATUS == 3):
                HEATING_STATUS = STATUS_ADV
                timer.reset_one(timer.HEAT)
                HTG_ADV_STATUS = timer.add_adv(timer.HEAT)
                status_str = "ADV"
            elif (HTG_BUTTON_STATUS == 4):
                HEATING_STATUS = STATUS_OFF
                timer.remove_category(timer.HEAT)
                status_str = "OFF"
            else:
                HEATING_STATUS = STATUS_RUN
                timer.reset_one(timer.HEAT)
                status_str = "RUN"

        lcd.lcd_on()

        logstr = "Heating button pressed - status: " + status_str
        log.log_message(logstr, log.DEBUG)
        print_next_action(timer.HEAT)

def wtr_button_press(channel):
    global WTR_BUTTON_STATUS, WATER_STATUS
    global WTR_ADV_STATUS
    status_str = ""

    if (debounce_input(channel, GPIO.PUD_UP)):
        if (lcd.read_backlight()):  # If True then backlight already on
            WTR_BUTTON_STATUS += 1
            if (WTR_BUTTON_STATUS > 5):
                WTR_BUTTON_STATUS = 2   # Loop back round again

            if (WTR_BUTTON_STATUS == 2):
                WATER_STATUS = STATUS_PLUSONE
                timer.reset_one(timer.WATER)
                timer.add_hour(timer.WATER)
                status_str = "+1H"
            elif (WTR_BUTTON_STATUS == 3):
                WATER_STATUS = STATUS_ADV
                timer.reset_one(timer.WATER)
                WTR_ADV_STATUS = timer.add_adv(timer.WATER)
                status_str = "ADV"
            elif (WTR_BUTTON_STATUS == 4):
                WATER_STATUS = STATUS_OFF
                timer.remove_category(timer.WATER)
                status_str = "OFF"
            else:
                WATER_STATUS = STATUS_RUN
                timer.reset_one(timer.WATER)
                status_str = "RUN"

        lcd.lcd_on()

        logstr = "Water button pressed - status: " + status_str
        log.log_message(logstr, log.DEBUG)
        print_next_action(timer.WATER)

def print_next_action(mode):
    index, on_off = timer.find_next(mode)
    SCHEDCOPY = timer.get_schedcopy()
    if (index != 666):
        if (on_off == Timer.TURN_OFF):
            action = "OFF"
            action_time = SCHEDCOPY[index][1].strftime('%H:%M')
        else:
            action = "ON"
            action_time = SCHEDCOPY[index][0].strftime('%H:%M')
    else:
        action = "No action"
        action_time = "NEVER"

    if (mode == timer.HEAT):
        logstr = "HEAT Next action: " + action + " @ " + action_time
    else:
        logstr = "WATER Next action: " + action + " @ " + action_time
    log.log_message(logstr, log.DEBUG)

def heating_on():
    relay.relay_on(relay.HTG_RELAY)
    led.led_on(led.HTG_LED)
    lcd.lcd_on()

def heating_off():
    relay.relay_off(relay.HTG_RELAY)
    led.led_off(led.HTG_LED)
    lcd.lcd_on()

def water_on():
    relay.relay_on(relay.WTR_RELAY)
    led.led_on(led.WTR_LED)
    lcd.lcd_on()

def water_off():
    relay.relay_off(relay.WTR_RELAY)
    led.led_off(led.WTR_LED)
    lcd.lcd_on()

if __name__ == '__main__':
    main()

    log.log_message("Stopping schedule", log.INFO)
    lcd.lcd_clear()
    lcd.lcd_override_off()
    ClockTimer.cancel()
    timer.cancel_timer()
    GPIO.cleanup()
