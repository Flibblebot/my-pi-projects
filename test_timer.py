from datetime import datetime, timedelta
from timer_class import Timer

timer = Timer()
today_str = datetime.today().strftime('%d/%m/%y')
SCHEDCOPY = timer.get_schedcopy()

for hour in range(0,24):
	for minute in range (0,60):
		timestr = str(hour).zfill(2) + ":" + str(minute).zfill(2)
		current_time = datetime.strptime(today_str + " " + timestr, '%d/%m/%y %H:%M')
		index, on_off = timer.find_next_test(timer.HEAT, current_time)
		if (index != 666):
			if (on_off == Timer.TURN_OFF):
				action = "OFF"
				time = SCHEDCOPY[index][1].strftime('%H:%M')
			else:
				action = "ON"
				time = SCHEDCOPY[index][0].strftime('%H:%M')
		else:
			action = "No action"
			time = "NEVER"
		print(timestr + "\tHEAT Next action: " + action + " @ " + time)

timer.reset()
for hour in range(0,24):
	for minute in range (0,60):
		timestr = str(hour).zfill(2) + ":" + str(minute).zfill(2)
		current_time = datetime.strptime(today_str + " " + timestr, '%d/%m/%y %H:%M')
		index, on_off = timer.find_next_test(timer.HEAT, current_time)
		if (index != 666):
			if (on_off == Timer.TURN_OFF):
				action = "OFF"
				time = SCHEDCOPY[index][1].strftime('%H:%M')
			else:
				action = "ON"
				time = SCHEDCOPY[index][0].strftime('%H:%M')
		else:
			action = "No action"
			time = "NEVER"
		print(timestr + "\tHEAT Next action: " + action + " @ " + time)

